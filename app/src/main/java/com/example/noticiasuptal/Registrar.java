package com.example.noticiasuptal;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Registrar extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrar);
Button guardar=(Button)findViewById(R.id.btn_regis);
        final EditText username=(EditText)findViewById(R.id.pass);

        final EditText contrase1=(EditText)findViewById(R.id.contra1);

        final EditText contrase2=(EditText)findViewById(R.id.contra2);
guardar.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
if(username.getText().toString()==""){
    username.setSelectAllOnFocus(true);
    username.requestFocus();
    return;
}
if(contrase1.getText().toString()==""){
    contrase1.setSelectAllOnFocus(true);
    contrase1.requestFocus();
    return;
}
        if(contrase2.getText().toString()==""){
            contrase2.setSelectAllOnFocus(true);
            contrase2.requestFocus();
            return;
        }
if(!contrase1.getText().toString().equals(contrase2.getText().toString())){
    Toast.makeText(Registrar.this,"Las Contraseñas no coinciden Bro",Toast.LENGTH_SHORT).show();
    return;
}
ServicioPeticion service =api.getApi(Registrar.this).create(ServicioPeticion.class);
        Call<Registro_Usuario>registro_usuarioCall=service.registrarUsuario(username.getText().toString(),contrase1.getText().toString());
        registro_usuarioCall.enqueue(new Callback<Registro_Usuario>() {
            @Override
            public void onResponse(Call<Registro_Usuario> call, Response<Registro_Usuario> response) {
                Registro_Usuario peticion = response.body();
                if(response.body()==null){
                    Toast.makeText(Registrar.this,"Ocurrio un error, intentalo despues",Toast.LENGTH_SHORT).show();
                    return;
                }
                if(peticion.estado=="true"){
                    startActivity(new Intent(Registrar.this,MainActivity.class));
                            Toast.makeText(Registrar.this,"Datos Guardados YEIIII",Toast.LENGTH_LONG).show(); }
                else{
                    Toast.makeText(Registrar.this,peticion.detalle,Toast.LENGTH_LONG).show();
                }
            }


            @Override
            public void onFailure(Call<Registro_Usuario> call, Throwable t) {
                Toast.makeText(Registrar.this,"Error Bro",Toast.LENGTH_LONG).show();
            }
        });


    }
});


    }
    public void btn(View view){
        Intent no = new Intent(Registrar.this, MainActivity.class);
        startActivity(no);

    }

}
